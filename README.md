# Iron OS
Bear metal OS in Rust for x86_64 architecture.

```
cargo install bootimage
rustup +nightlycomponent add llvm-tools-preview
cargo +nightly xbuild
cargo +nightly bootimage
qemu-system-x86_64 -drive format=raw,file=
```
